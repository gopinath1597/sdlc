package com.maxim.jms.producer.service;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.maxim.jms.producer.controller.ProducerController;
import com.maxim.jms.producer.model.Vendor;
import com.maxim.jms.producer.sender.MessageSender;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MessageService {
	private static Logger logger = LogManager.getLogger(MessageService.class.getName());
	@Autowired
	private MessageSender messageSender;
	public void process(Vendor vendor) {
		// TODO Auto-generated method stub
		logger.info("Processing Vendor Object");
		Gson gson = new Gson();
		String json = gson.toJson(vendor);
		logger.info("After JSon Convertion"+json);
		logger.info("Before sending Json to the messageSender"+json);
		messageSender.sendMessage(json);
	} 

}
