package com.maxim.jms.producer.sender;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.JmsException;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import com.maxim.jms.producer.service.MessageService;

@Service
public class MessageSender {
	private static Logger logger = LogManager.getLogger(MessageService.class.getName());
	@Autowired
	JmsTemplate jmsTemplate;
	
	public void sendMessage(String json) {
		// TODO Auto-generated method stub
		try
		{
			jmsTemplate.convertAndSend(json);
			logger.info("Message sent to the queue");
		}
		catch(JmsException e)
		{
			logger.error("Messag error"+json);
		}
	}
}
