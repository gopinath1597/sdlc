<!DOCTYPE html>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
<head>
<meta charset="utf-8">
<title>Vendor Entry</title>
<link rel="stylesheet" href="<c:url value='/css/style.css'/>"/>
</head>
<body>
	<div>
		<h2>Vendor Entry</h2>
	</div>
	<div id="form">
		<form:form modelAttribute="vendor" action="vendor">
		   <div id="message" class="message">
		    <c:if test="${!empty message} }">
		    	<c:out value = "${message}"/>
		    </c:if>
		    
		   </div>
		   	<fieldset>
				<legend>Vendor Entry</legend>
				<div>
					<label for="VendorName">VendorName</label>
					<form:input path="vendorName" />
				</div>
				<div>
					<label for="FirstName">FirstName</label>
					<form:input path="firstName" />
				</div>
				<div>
					<label for="LastName">LastName</label>
					<form:input path="lastName" />
				</div>
				<div>
					<label for="address">Address</label>
					<form:input path="address" />
				</div>
				<div>
					<label for="City">City</label>
					<form:input path="city" />
				</div>
				<div>
					<label for="State">State</label>
					<form:select path="state">
						<form:option value="Alabama">Alabama</form:option>
						<form:option value="Texas">Texas</form:option>
					</form:select>					
				</div>
				<div>
					<label for="ZipCode">ZipCode</label>
					<form:input path="zipCode" />
				</div>
				<div>
					<label for="Email">Email</label>
					<form:input path="email" />
				</div>
				<div>
					<label for="PhoneNumber">PhoneNumber</label>
					<form:input path="phoneNumber" />
				</div>
				<div>
					<input type="submit" value="Submit" />
				</div>
			</fieldset>
		</form:form>
	</div>
</body>
</html>
