package test.com.maxim.listener;

import static org.junit.Assert.*;
import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;

import javax.jms.JMSException;
import javax.jms.TextMessage;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.maxim.listener.ConsumerListener;

public class ConsumerListenerTest {

	private TextMessage message;
	private ApplicationContext applicationContext;
	private ConsumerListener listener;
	private String json = "{vendorName:\"Microsofttest2\",firstName:\"BobTest2\",lastName:\"SmithTest2\",address:\"123 Main test2\",city:\"TulsaTest2\",state:\"OKTest2\",zip:\"71345Test2\",email:\"Bob@microsoft.test2\",phoneNumber:\"test-123-test2\"}";
	
	@Before
	public void setUp() throws Exception 
	{
		applicationContext = new ClassPathXmlApplicationContext("/spring/application-config.xml");
		listener = (ConsumerListener)applicationContext.getBean("consumerListener");
		message = createMock(TextMessage.class);
	}

	@After
	public void tearDown() throws Exception {
	((ConfigurableApplicationContext)applicationContext).close();
	}

	@Test
	public void testOnMessage() throws JMSException {	
		expect(message.getText()).andReturn(json);
		replay(message);
		listener.onMessage(message);
		assertNotNull(message);
		verify(message);
		
	}

}
